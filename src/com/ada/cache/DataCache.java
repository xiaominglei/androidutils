package com.ada.cache;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by 年高 on 2015/4/11.
 */
public class DataCache<T> {

    public final static String FILDER_GLOBAL = "FILDER_GLOBAL";

    public void save(Context ctx, ArrayList<T> data, String name) {
        save(ctx, data, name, "");
    }

    public void saveGlobal(Context ctx, ArrayList<T> data, String name) {
        save(ctx, data, name, FILDER_GLOBAL);
    }

    public void delete(Context ctx, String name) {
        File file = new File(ctx.getFilesDir(), name);
        if (file.exists()) {
            file.delete();
        }
    }

    private void save(Context ctx, ArrayList<T> data, String name, String folder) {
        if (ctx == null) {
            return;
        }

        File file;
        if (!folder.isEmpty()) {
            File fileDir = new File(ctx.getFilesDir(), folder);
            if (!fileDir.exists() || !fileDir.isDirectory()) {
                fileDir.mkdir();
            }
            file = new File(fileDir, name);
        } else {
            file = new File(ctx.getFilesDir(), name);
        }

        if (file.exists()) {
            file.delete();
        }

        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(data);
            oos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<T> load(Context ctx, String name) {
        return load(ctx, name, "");
    }

    public ArrayList<T> loadGlobal(Context ctx, String name) {
        return load(ctx, name, FILDER_GLOBAL);
    }

    private ArrayList<T> load(Context ctx, String name, String folder) {
        ArrayList<T> data = null;

        File file;
        if (!folder.isEmpty()) {
            File fileDir = new File(ctx.getFilesDir(), folder);
            if (!fileDir.exists() || !fileDir.isDirectory()) {
                fileDir.mkdir();
            }
            file = new File(fileDir, name);
        } else {
            file = new File(ctx.getFilesDir(), name);
        }

        if (file.exists()) {
            try {
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
                data = (ArrayList<T>) ois.readObject();
                ois.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (data == null) {
            data = new ArrayList<T>();
        }

        return data;
    }
}