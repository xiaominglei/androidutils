package com.ada.cache;

import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.content.Context;

public class SimpleDataCache<ItemType>  {

	private String dir;
    public SimpleDataCache(String dir) {
		super();
		this.dir = dir;
	}
    public void delete(Context ctx, String name) {
        File file = new File(ctx.getFilesDir(), name);
        if (file.exists()) {
            file.delete();
        }
    }
	public  void saveAccount(Context ctx ,  ItemType data,String key) {
        File file = new File(ctx.getFilesDir(), key);
        if (file.exists()) {
            file.delete();
        }

        try {
            ObjectOutputStream oos = new ObjectOutputStream(ctx.openFileOutput(key, Context.MODE_PRIVATE));
            oos.writeObject(data);
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  ItemType loadAccount(Context ctx,String key) {
    	ItemType data = null;
        File file = new File(ctx.getFilesDir(), key);
        if (file.exists()) {
            try {
                ObjectInputStream ois = new ObjectInputStream(ctx.openFileInput(key));
                data = (ItemType) ois.readObject();
                ois.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (data == null) {
            //data = new T();
        }

        return data;
    }
}
